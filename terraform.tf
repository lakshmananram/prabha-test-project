terraform {
  backend "s3" {
    bucket = "y8nutestforcicd"
    key    = "terraform/state"
    region = "us-east-1"
  }
}
